<?php
require 'buildings.array.php';
require 'Hotel.Class.php';

$hotels = [];
foreach($buildings as $hotel){              //make array for object
  $hotels[] = new Hotel($hotel['title'],
                        $hotel['shortDescription'],
                        $hotel['description'],
                        $hotel['picture']);
  
  }

$i=1; // counter for hotels

$lookhotel = $_COOKIE['viewed'];  //accept cookie
$lookhotel = json_decode($lookhotel,true);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Отели</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<!--Content block start-->

<!-- Watchrds start-->
<?php if(isset($lookhotel)):?>
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Просмотреные</th>
    </tr>
  </thead>
  <tbody>
    <?php 
   
    foreach($lookhotel as $key): ?>  
    <tr>
      <td><?=$hotels[$key]->title?></td>
    </tr>

       <?php  
          if($i==5) // watchet all hotels
          echo "<center><b><h2>Вы просмотрели все отели</h2></b></center>"; $i++;?>

    <?php endforeach; ?>
  </tbody>
</table><a href="deleteStory.php">Очистить историю</a>

<?php endif; ?>
<!-- Watchrds end-->

<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th></th>
      <th scope="col">Название</th>
      <th scope="col">Краткое описание</th>
      <th scope="col"></th>
    </tr>
  </thead>

  <tbody>
    <?php foreach($hotels as $key => $hotel): ?>  
    <tr>
      <th scope="row"><?=$i++?></th>
      <td><img src="<?=$hotel->picture;?> " width="100" height="100"></td>
      <td><?=$hotel->title?></td>
      <td><?=$hotel->shortDescription?></td>
      <td><a href="detail.php?key=<?=$key?>">Подробнее</a></td>
    </tr>
    <?php endforeach;?>
  </tbody>
</table>


<!--Content blocck end-->

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->


</body>
</html>