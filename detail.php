<?php
require 'buildings.array.php';
require 'Hotel.Class.php';


$hotelId = $_GET['key'];//accept key hotels

if(isset($buildings[$hotelId])) { 
    $vatchedHotels = $_COOKIE['viewed'];
    if($vatchedHotels) {
        $vatchedHotels = json_decode($vatchedHotels, true);
    }
    if(is_array($vatchedHotels)) {
        if(!in_array($hotelId, $vatchedHotels)) {
            $vatchedHotels[] = $hotelId;
        }
    }else {
        $vatchedHotels[] = $hotelId;
    }
    setcookie('viewed', json_encode($vatchedHotels), time()+3600*24*356);//install cookie
    
} 
?>  


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Отель</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<!--Content block start-->
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Фото</th>
      <th scope="col">Название</th>
      <th scope="col">Подробное описания</th>
    </tr>
  </thead>
<!--Watchet detail-->
  <tbody>
    <tr>
      <td><img src="<?=$buildings[$hotelId]['picture'];?> " width="100" height="100"></td>
      <td><?=$buildings[$hotelId]['title'];?></td>
      <td><?=$buildings[$hotelId]['description'];?></td>
    </tr>
  </tbody> 
</table>

<a href="index.php"><center><h3>Назад</h3></center></a>
<!--Content blocck end-->

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->
</body>
</html>